// console.log("Hello World");



let numA = 0;

/* 
if statement
Executes a statement if a specified condition is true
syntax

if(condition){
    code block(statement);
}

*/

if(numA < 0){
    console.log("Hello");
}
// The result of the expression in the if statement must result to "true", else it will not run the statement inside.

// for checking the value
console.log(numA < 0);

let city = "New York";

if(city === "New York"){
    console.log("Welcome to New York City!");
}

// else if clause
/* 
 - Executes a statement if previous condition are false and if the specified condition is true.
 - The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program
*/

let numH = 0;
    if(numH < 0){
        console.log("Hello");
    }
    else if (numH > 0){
        console.log("world");
    }

city = "Manila";
if(city === "New York"){
    console.log("Welcome to New York City");
}
else if(city === "Tokyo"){
    console.log("Welcome to Tokyo, Japan");
}


/* 
else statement
executes a statement if all other condition are false
the 'else' statement is optional and can be added to capture any other result to change the flow of grow

*/


if(numH < 0){
    console.log("Hello");
}
else if(numH < 1){
    console.log("World");
}
else{
    console.log("Again");
}


if(city === "New York"){
    console.log("Welcome to New York City.");
}
else if(city === "Tokyo"){
    console.log("Welcom to Tokyo, Japan.");
}
else{
    console.log("City is not included in the list.");
}


/*
				Scenario: We want to determine intensity of a typhone based on its wind speed.
					Not a Typoon - Wind speed is less than 30. 
					Tropical Depression - Wind speed is greater than or equal to 61.
					Tropical Storm - Wind speed is between 62 to 88.
					Severe Tropical Storm - Wind speed is between 89 to 117.
					Typoon - Wind speed is greater than or equal to 118.

			*/


/* 
if(windSpeed < 30){
    console.log("Not a Typoon.");
}
else if(windSpeed <= 61){
    console.log("Tropical Depression");
}
else if(windSpeed >= 62 && windSpeed <= 88 ){
    console.log("Tropical Storm");
}
else if(windSpeed >= 89 && windSpeed <= 117 ){
    console.log("Severe Tropical Storm");
}
else if(windSpeed >= 118){
    console.log("Typhoon");
}
else{
    console.log("City is not included in the list.");
}

 */


let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed){
    if(windSpeed < 30){
        return "Not a Typoon yet."
    }
    else if(windSpeed <= 61){
        return "Tropical Depression detected"
    }
    else if(windSpeed >= 62 && windSpeed <= 88 ){
        return "Tropical Storm detected"
    }
    else if(windSpeed >= 89 && windSpeed <= 117 ){
        return "Severe Tropical Storm detected"
    }
    else{
        return "Typhoon detected"
    }
}

message = determineTyphoonIntensity(110);
console.log(message);



// console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code
if(message === "Severe Tropical storm detected"){
    console.warn(message);
}

/* 
[SECTION] Truthy and Falsy
In JavaScript a "truthy" value is a value that is considered true when encountered in a Boolean Context

Falsy values / exception for truthy:
1. false
2. 0
3. ""
4. null
5. negative "-"
6. undefined
7. Nan


*/

let isMarried = true;

// Truthy examples:

if (true){
    console.log("Truthy");
}

if(1){
    console.log("Truthy");
}

if([]){
    console.log("Truthy");
}

// falsy examples:
if(false){
    console.log("Falsy");
}

if(0){
    console.log("Falsy");
}

if(undefined){
    console.log("Falsy");
}

if(isMarried){
    console.log("Truthy");
}